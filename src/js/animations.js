/* eslint-disable no-unused-vars */
import { TimelineLite, Power0, Power2, CSSPlugin} from './gsap-modules';
import {DrawSVGPlugin} from './gsap-bonus/DrawSVGPlugin.js';
const plugins = [DrawSVGPlugin, CSSPlugin];
/* eslint-enable no-unused-vars */

function charAnimation(section='frontpage') {
  let tl = new TimelineLite({});
  const character = document.querySelector(`#${section} .section-character`);
  const pic = character.querySelector('.body-bg');
  const contours = character.querySelectorAll('.body-contour path');
  // const artefacts = character.querySelector('.body-bg.artefacts');

  // const charDuration = section === 'frontpage'? 5: 4;

  tl
    .set(character, {opacity: 1})
    .staggerFrom(contours, 4, {drawSVG: '0%',  ease: Power0.easeNone})
    .to(contours, 0.5, {opacity: 0})
    .to(pic, 0.5, {opacity: 1}, '-=0.5');

  // if (section === 'not-working' && artefacts) {
  //   tl.to(artefacts, .5, {opacity: 1});
  // }

  return tl;
}

function stripesAnimation(section='frontpage') {
  let tl = new TimelineLite({});
  const arr = document.querySelectorAll(`#${section} .bg-stripes span`);
  tl
    .staggerTo(arr, 0.7, {
      left: 0,
      ease:Power2.easeOut
    }, 0.15, 0);
  return tl;
}

export {charAnimation, stripesAnimation};
