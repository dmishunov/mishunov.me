// import '../css/section.css';
import(/* webpackChunkName: 'section-css' */'../css/section.css');

import {TimelineLite, Power2, CSSPlugin} from './gsap-modules';
import {TextPlugin} from 'gsap/TextPlugin';

// import * as Utils from './utils';
import * as Animations from './animations';

/* eslint-disable no-unused-vars */
const plugins = [TextPlugin, CSSPlugin];
/* eslint-enable no-unused-vars */

function _sectionsTitleRevealAnimation() {
  let tl = new TimelineLite({});
  const el = document.querySelector('#sections-title .svg-container');
  tl.to(el, 1, {autoAlpha: 1});
  return tl;
}

function _sectionsTitleEnterAnimation(section='frontpage') {
  // if (usedSections.indexOf(section) === -1) {
  //   usedSections.push(section);
  // }

  let tl = new TimelineLite({});
  const el = document.querySelector('#sections-title .placeholder');
  const texts = [
    'sometimes I',
    'occasionally I',
    'at times I',
    'on occasion I',
    'every so often I',
    'now and then I',
    'once in a while I',
    'here and there I'
  ];
  const current = el.innerHTML.trim();
  const textsToPickFrom = texts.filter( (text) => text!== current );
  const text = textsToPickFrom[Math.floor(Math.random()*textsToPickFrom.length)];
  tl
    .to(el, 2, {text:{value:text}, ease:Power2.easeOut})
    .set(el, {className: `placeholder ${section}`});
  // .to(document.getElementById('eraser'), 0.3, {autoAlpha: 0});
  return tl;
}

export function runRepetitiveSectionAnimation(section='frontpage', standalone=true) {
  const tl = new TimelineLite({
    onComplete: function() {
      if (standalone) {
        window.dispatchEvent(new CustomEvent('base-section-animation-ready', {detail: {type: 'repetitive'}}));
      }
    }
  });
  const title = document.getElementById('sections-title');
  const svg = title.querySelector('.svg-container');

  tl
    .set(title, {autoAlpha: 1})
    .add(_sectionsTitleEnterAnimation(section))
    .set(svg, {yPercent: 0})
    .to(svg, 0.3, {autoAlpha: 1});

  return tl;
}

export function sectionAnimation(section) {

  performance.mark('loading-end');
  performance.measure('loading', 'loading-start', 'loading-end');

  document.getElementById(section).setAttribute('ready', 'true');

  let tl = new TimelineLite({
    onComplete: ()=> {
      document.body.setAttribute('ready', 'true');
      // document.getElementById(section).setAttribute('ready', 'true');
    }
  });
  const title = document.getElementById('sections-title');
  const onBaseAnimationEnd = function() {
    window.dispatchEvent(new CustomEvent('base-section-animation-ready', {detail: {type: 'initial'}}));
  };
  let delay ='+=0';

  tl.set(document.body, {opacity: 1});

  if (!title.getAttribute('ready')) {
    tl.set(title, {autoAlpha: 1});
  }

  tl.add(runRepetitiveSectionAnimation(section, false));

  if (!title.hasAttribute('ready')) {
    tl.add(_sectionsTitleRevealAnimation());
    delay = '-=1.2';
  }

  document.body.dispatchEvent(new CustomEvent('section-ready'));

  tl
    .add(Animations.charAnimation(section), delay)
    .call(onBaseAnimationEnd)
    .add(Animations.stripesAnimation(section));

  title.setAttribute('ready', 'true');

  return tl;
}
