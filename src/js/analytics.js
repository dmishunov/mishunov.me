function _isDNTEnabled() {
  return navigator.doNotTrack === '1';
}
function _setUpAnalytics() {
  let analytics = document.createElement('script');
  analytics.async = true;
  analytics.src = 'https://www.googletagmanager.com/gtag/js?id=UA-29081029-1';
  const analyticsListener = function() {
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-29081029-1');
    analytics.removeEventListener('load', analyticsListener);
  };

  analytics.addEventListener('load', analyticsListener);
  document.body.appendChild(analytics);
}

export function bootstrapAnalytics() {
  // if(!_isDNTEnabled()) {
  //   _setUpAnalytics();
  // } else if (document.cookie.indexOf('allow_analytics=true') !== -1) {
  //   _setUpAnalytics();
  // } else {
  //   import('./dnt').then(view => view.bootstrapDNT(_setUpAnalytics));
  // }
  _setUpAnalytics();
}


