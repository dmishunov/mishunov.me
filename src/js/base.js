import {Power0, Power2, TimelineLite, CSSPlugin} from './gsap-modules';
import { Elastic } from 'gsap/all';

import * as ExitTransitions from './exit-transitions';
import * as debounce from 'lodash/debounce';

import './modernizr-min';

const plugins = [CSSPlugin];
let contactInfoToggleAnimationBootstrapTimeline;
const transitionOptions = [
  'push',
  'eraser'
];
// let usedSections = [];
let initLoad = true;
// let builtTimelines = [];

const Frontpage = import('./frontpage');
const Section = import('./section');

const _scrollToSection = function(el) {
  if(getComputedStyle(el).scrollSnapAlign === undefined) {
    window.scrollTo({top: el.offsetTop, left: 0, behavior: 'smooth'});
  }
};
const _intersectionEntranceHandler = debounce(function(el) {
  _scrollToSection(el);
  const id = el.id;
  window.requestAnimationFrame(function() {
    document.body.setAttribute('current-section', id);
    if(!el.getAttribute('ready')) {
      // usedSections.push(id);
      switch (id) {
        case 'frontpage':
          window.history.pushState({}, '', window.location.origin);
          document.title = 'Denys Mishunov: developer, writer, speaker';
          let tl = new TimelineLite({});
          tl.set(document.getElementById('sections-title'), {autoAlpha: 0});
          if (!initLoad) {
            Frontpage.then(view => view.sectionAnimation(true) );
          }
          break;
        default:
          window.history.pushState({}, '', `#${id}`);
          document.title = `Denys Mishunov. ${id.charAt(0).toUpperCase() + id.slice(1)}`;
          if (!initLoad) {
            Section.then(view => view.sectionAnimation(id) );
          }
          // runInitialSectionAnimation(id);
          break;
      }
    } else {
      switch (id) {
        case 'frontpage':
          window.history.pushState({}, '', window.location.origin);
          document.title = 'Denys Mishunov: developer, writer, speaker';
          let tl = new TimelineLite({});
          tl.set(document.getElementById('sections-title'), {autoAlpha: 0});
          break;
        default:
          window.history.pushState({}, '', `#${id}`);
          document.title = `Denys Mishunov. ${id.charAt(0).toUpperCase() + id.slice(1)}`;
          if (!initLoad) {
            Section.then(view => view.runRepetitiveSectionAnimation(id, true) );
          }
          // runRepetitiveSectionAnimation(id, true);
          break;
      }
    }
    initLoad = false;
  });
}, 500);

const _intersectionExitHandler = function(el) {
  // if (builtTimelines[el.id]) {
  //   window.requestAnimationFrame(() => {
  //     builtTimelines[el.id].seek(builtTimelines[el.id].totalDuration(), false);
  //   });
  // }
};

export function contactInfoToggleAnimationBootstrap() {
  const toggle = document.getElementById('contacts-toggle');
  const ripple = toggle.querySelector('.ripple');
  const footer = document.querySelector('footer');
  const contactsList = document.querySelector('footer ul');
  const size = window.innerHeight;
  let tl = new TimelineLite({});

  toggle.addEventListener('click', (ev) => {
    if (contactInfoToggleAnimationBootstrapTimeline && !toggle.getAttribute('data-opened')) {
      contactInfoToggleAnimationBootstrapTimeline.seek(0);
      toggle.setAttribute('data-opened', true);
    } else if (!contactInfoToggleAnimationBootstrapTimeline && !toggle.getAttribute('data-opened')) {
      contactInfoToggleAnimationBootstrapTimeline = tl
      .set(footer, {zIndex: 9})
      .set(ripple, {autoAlpha:1})
      .set(contactsList, {autoAlpha:1})
      .to(ripple, 1, {scale: size, ease: Power2.easeOut})
      .to(ripple, 0.5, {autoAlpha: 0}, '-=0.5')
      .to(contactsList, 1, {scale: 1, ease: Elastic.easeOut.config(1, 0.4)}, '-=1');

      toggle.setAttribute('data-opened', true);
    } else if (contactInfoToggleAnimationBootstrapTimeline && toggle.getAttribute('data-opened')) {
      contactInfoToggleAnimationBootstrapTimeline = undefined;

      tl
      .to(contactsList, .3, {scale: 0, ease: Elastic.easeIn.config(1.5, 0.75)})
      .set(contactsList, {autoAlpha:0})
      .set(ripple, {scale: 0})
      .set(footer, {zIndex: 5});

      toggle.removeAttribute('data-opened');
    }
  });
}

export function setNavigationScrolling() {
  const navItems = document.querySelectorAll('nav a');
  // const handler = function(item) {
  //   scrollTo(item.getAttribute('href'));
  // };
  for(let i =0, l = navItems.length; i<l; ++i) {
    navItems[i].addEventListener('click', scrollTo);
  }
}

export function intersectionObserverSetup() {
  const sections = document.querySelectorAll('section.sections-container');
  const config = {
    rootMargin: '0px',
    threshold: 0.6
  };

  let observer = new IntersectionObserver(function (entries) {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        if (entry.intersectionRatio > config.threshold) {
          _intersectionEntranceHandler(entry.target);
        } else {
          _intersectionExitHandler(entry.target);
        }
      } else {
        _intersectionExitHandler(entry.target);
        // console.log(`${entry.target.id} is NOT INTERSECTING`);
      }
    });
  }, config);

  sections.forEach((section) => observer.observe(section) );
}

export function scrollTo(ev) {
  ev.preventDefault();

  if (window.location.hash === ev.target.getAttribute('href')) {
    return;
  }

  const sectionName = ev.target.getAttribute('href').slice(1);
  const sectionIn = document.getElementById(sectionName);
  const sectionOut = document.getElementById(`${document.body.getAttribute('current-section')}`);
  const picIn = sectionIn.querySelector('.body-bg');
  const picOut = sectionOut.querySelector('.body-bg');
  // const svg = sectionOut.id === 'frontpage'? sectionOut.querySelector('#intro'): document.querySelector('#sections-title .svg-container');
  const svg = document.querySelector('#sections-title .svg-container');
  const nav = document.querySelector('nav');
  const referenceRect = ev.target.getBoundingClientRect();
  const transitionStyle = (function() {
    if (transitionOptions.length && sectionOut.id !== 'frontpage' && window.matchMedia('(orientation: landscape), (min-aspect-ratio: 768/1024)').matches) {
      return transitionOptions.splice(Math.floor(Math.random()*transitionOptions.length), 1)[0];
    }
    return undefined;
  })();
  const setupRipple = function (el) {
    if (transitionStyle) {
      el.classList.remove('lift');
    } else {
      el.classList.add('lift');
    }
    el.style.backgroundColor = `var(--${sectionName}-bg-color)`;
    if (window.matchMedia('(orientation: landscape), (min-aspect-ratio: 768/1024)').matches) {
      el.style.width = '100vw';
      el.style.top = `${referenceRect.top}px`;
      el.style.height = `${referenceRect.height}px`;
      el.style.left = '0';
    } else {
      el.style.width = `${referenceRect.width}px`;
      el.style.left = `${referenceRect.left}px`;
      el.style.height = '100vh';
      el.style.top = '0';
    }
  };
  const scale = (function() {
    if (window.matchMedia('(orientation: landscape), (min-aspect-ratio: 768/1024)').matches) {
      return window.innerHeight/referenceRect.height;
    } else {
      return window.innerWidth/referenceRect.width;
    }
  })();

  let ripple = sectionOut.querySelector('.nav-ripple');
  if (!ripple) {
    ripple = document.createElement('span');
    ripple.className = 'nav-ripple';
    setupRipple(ripple);
    sectionOut.appendChild(ripple);
  } else {
    setupRipple(ripple);
  }
  let tl = new TimelineLite({
    onComplete: () => {
      let beforeNavTL = new TimelineLite({});
      let afterNavTL = new TimelineLite({});
      const sectionElStripes = sectionIn.querySelectorAll('.bg-stripes span');
      const sectionTitle = document.getElementById('sections-title');

      if (sectionIn.getAttribute('ready')) {
        beforeNavTL.set(picIn, {xPercent: -100, opacity: 0});
      }

      beforeNavTL
        .set(sectionTitle, {className: '+= lifted'})
        .set([sectionElStripes, nav], {autoAlpha: 0})
        .set(sectionIn, {className: '+=animating'})
        .set(ripple, {scale: 1, autoAlpha: 0});

      const baseSectionAnimationReadyHandler = function (ev) {

        window.removeEventListener('base-section-animation-ready', baseSectionAnimationReadyHandler);

        if (ev.detail && ev.detail.type === 'repetitive') {
          afterNavTL
            .to(picIn, .3, {opacity: 1, ease: Power0.easeNone})
            .to(picIn, 1.5, {xPercent: 0, ease: Elastic.easeOut.config(1, 0.75)}, '-=.3')
            .addLabel('start', '-=0.3');
        } else {
          afterNavTL.addLabel('start');
        }

        afterNavTL
          .staggerTo(sectionElStripes, .4, {autoAlpha: 1, ease:Power0.easeNone}, 0.1, 'start')
          .to(nav, .3, {autoAlpha: 1}, '-=0.3')
          .set(sectionIn, {className: '-=animating'})
          .set(picOut, {autoAlpha: 1, rotation: 0, xPercent: 0, scale: 1})
          .set(sectionTitle, {className: '-= lifted'});
        if (sectionOut.querySelector('.transition-el')) {
          tl.set(sectionOut.querySelector('.transition-el'), {autoAlpha: 0});
        }
      };

      window.addEventListener('base-section-animation-ready', baseSectionAnimationReadyHandler);

      window.location=`#${sectionName}`;
    }
  });

  tl
    .set(ripple, {autoAlpha: 1})
    .addLabel('start')
    .to(svg, 0.4, {yPercent: -100, ease: Power0.easeNone}, 'start')
    .to(svg, 0.3, {opacity: 0}, 'start');

  if (window.matchMedia('(orientation: landscape), (min-aspect-ratio: 768/1024)').matches) {
    tl.to(ripple, 0.3, {scaleY: scale, top: `calc(50% - ${referenceRect.height/2}px)`, ease: Power2.easeOut}, 'start');
  } else {
    tl.to(ripple, 0.3, {scaleX: scale, left: `calc(50% - ${referenceRect.width/2}px)`, ease: Power2.easeOut}, 'start');
  }

  tl.to(nav, 0.2, {autoAlpha: 0}, 'start');

  if (sectionOut.id !== 'frontpage' && !sectionIn.getAttribute('ready') && window.matchMedia('(min-width: 1024px)').matches) {
    switch(transitionStyle) {
      case 'eraser':
        tl.add(ExitTransitions.eraserExitAnimation(document.body.getAttribute('current-section'), sectionName));
        break;
      case 'push':
        tl.add(ExitTransitions.pushExitAnimation(document.body.getAttribute('current-section'), sectionName));
        break;
    }
  }

  document.body.setAttribute('current-section', sectionName);

  return tl;
}

// bootstrapFirstScreen();
// setNavigationScrolling();
