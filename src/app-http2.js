// import style from "./css/base.css";

performance.mark('loading-start');

let currentView;
let baseView;

// require('./src/js/gsap-modules');

if (window.location.hash === '') {
  currentView= import(/* webpackChunkName: "frontpage" */ './js/frontpage');
} else {
  currentView = import(/* webpackChunkName: "section" */ './js/section');
}

const loadDefered = function() {

  document.body.removeEventListener('section-ready', loadDefered);

  if (window.location.hash === '') {
    import(/* webpackChunkName: "section" */ './js/section').then((view) => {
      performance.mark('secondary-view-here');
    });
  } else {
    import(/* webpackChunkName: "frontpage" */ './js/frontpage').then((view) => {
      performance.mark('secondary-view-here');
    });
  }
  import(/* webpackChunkName: "basejs" */ './js/base').then(view => {
    performance.mark('base-view-here');
    view.intersectionObserverSetup();
    view.setNavigationScrolling();
    view.contactInfoToggleAnimationBootstrap();
  });
  // import(/* webpackChunkName: "analytics" */ './js/analytics').then(view => {
  //   performance.mark('analytics-view-here');
  //   view.bootstrapAnalytics();
  // });
};

document.body.addEventListener('section-ready', loadDefered);

currentView
.then(view => {
  performance.mark('main-view-here');
  // console.log('We have MAIN view');
  document.body.setAttribute('style', 'opacity: 1');
  view.sectionAnimation(window.location.hash.split('#')[1]);
});
