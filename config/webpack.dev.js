const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const postcssCustomProperties = require('postcss-custom-properties');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;

const pathToMainCss = require.resolve('../src/css/base.css');
const isModern = process.env.BROWSERSLIST_ENV === 'modern';
const modernAppEntry = [
  '@babel/runtime/regenerator',
  '@babel/register',
  'webpack-hot-middleware/client?reload=true',
  './src/app.js'
];
const legacyAppEntry = [
  './src/js/polyfills.js',
  '@babel/runtime/regenerator',
  '@babel/register',
  'webpack-hot-middleware/client?reload=true',
  './src/app.js'
];

module.exports = {
  // entry: isModern? { modernAppEntry, pathToMainCss }: { legacyAppEntry, pathToMainCss },
  entry: { legacyAppEntry, pathToMainCss },
  mode: 'development',
  output: {
    filename: '[name]-bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  devServer: {
    contentBase: 'dist',
    overlay: true,
    stats: {
      colors: true
    }
  },
  // optimization: {
  //   splitChunks: {
  //     chunks: "all"
  //   }
  // },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.css$/,
        exclude: pathToMainCss,
        use: [
          MiniCSSExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: pathToMainCss,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                postcssCustomProperties()
              ]
            }
          }
        ]
      },
      {
        test: /\.(jpg|png|svg|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HTMLWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCSSExtractPlugin({
      filename: '[name].css'
    }),
    new webpack.NamedModulesPlugin(),
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: [
        'exit-transitions-css',
        'section-css',
        'frontpage',
        'frontpage-css',
        'app',
        'pathToMainCss'],
    }),
    new BundleAnalyzerPlugin({
      generateStatsFile: true
    })
  ]
};
