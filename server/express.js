import path from 'path';
import express from 'express';
const fs = require('fs');
const spdy = require('spdy');
const https = require('https');
const http = require('http');
const expressStaticGzip = require('express-static-gzip');

const PORT = process.env.PORT || 8080;
const isProd = process.env.NODE_ENV === 'production';
const options = {
  key: fs.readFileSync(__dirname + '/server.key'),
  cert: fs.readFileSync(__dirname + '/server.crt')
};

var app = express();

if (!isProd) {
  const webpack = require('webpack');
  const config = require('../config/webpackH2.dev.js');
  const compiler = webpack(config);

  const webpackDevMiddleware = require('webpack-dev-middleware')(
    compiler,
    config.devServer
  );

  const webpackHotMiddlware = require('webpack-hot-middleware')(
    compiler,
    config.devServer
  );

  app.use(webpackDevMiddleware);
  app.use(webpackHotMiddlware);
  console.log('Middleware enabled');
}

// const staticMiddleware = express.static("dist");
// app.use(staticMiddleware);
app.use(expressStaticGzip('dist', {
  enableBrotli: true
}));

// app.listen(PORT, () => {
//   console.log(
//     `Server listening on http://localhost:${PORT} in ${process.env.NODE_ENV}`
//   );
// });

// app.get('/', (req, res) => {
//   res.push('/frontpage-css.css', {
//     status: 200, // optional
//     method: 'GET', // optional
//     request: {
//       accept: '*/*'
//     },
//     response: {
//       'content-type': 'text/css'
//     }
//   });
//   // stream.end('alert("hello from push stream!");');
//   // res.write();
//   res.end('<link rel="stylesheet" type="text/css" href="/frontpage.css">');
// });

// const httpsServer = https.createServer(options, app);
// httpsServer.listen(PORT);
const httpServer = http.createServer(app);
httpServer.listen(8080);

// spdy
//   .createServer(options, app)
//   .listen(PORT, () => console.log(
//     `Server listening on http://localhost:${PORT} in ${process.env.NODE_ENV}`
//   ));
